SendMail.MDB

The Access database included in this example is Access version 2000.

To enable you to test the database you will firstly need to access the Job Outcomes table (tblJobOutcomes) and uncheck some of the checkboxes in the ysnSentByMailToStaff field.

This field is updated by the SendMail procedure when mail has been sent to ensure that duplicates are not sent.

Once this has been done, you will also need to access the SendMail procedure and enter some valid e-mail address into the code.

In Public Sub SendMail()

where it states: strEmailAddress = "[Mail Addresses Go Here]"

change this to read something along the lines of:

strEmailAddress = "webmaster@anywhere.com; graham@nowhere.co.uk"

etc, etc....

You should now be able to send the formatted e-mails to your recipients.

Hopefully this should be a straight forward example.

If you have any problems feel free to contact me at:

webmaster@databasedev.co.uk

Regards

Graham